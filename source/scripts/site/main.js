$(window).on('load', function () {
    $('body').addClass('is-loaded'); 
}); 

const imageVertexShader = `
    #define PI 3.1415926535897932384626433832795
    
    uniform vec2 uOffset;
    uniform vec2 uViewportSizes;
    uniform float uScrollVelo;
    uniform float uTime;
    
    varying vec2 vUv;


    void main() {
        vUv = uv;

        vec4 newPosition = modelViewMatrix * vec4(position, 1.0);
        
        newPosition.x -= sin(newPosition.y / 1024. * PI + PI / 2.0) * uScrollVelo * 100.;
        newPosition.z -= sin(newPosition.x / 1440. * PI + PI / 2.0) * uScrollVelo * 150.;

        gl_Position = projectionMatrix * newPosition;
    }
`;

//image fragment shader
const imageFragmentShader = `
    uniform sampler2D uTexture;
    uniform float uAlpha;
    uniform vec2 uOffset;
    
    varying vec2 vUv;

    vec3 rgbShift(sampler2D textureimage, vec2 uv, vec2 offset) {
        float r = texture2D(textureimage, uv ).r;
        vec2 gb = texture2D(textureimage, uv ).gb;
        return vec3(r, gb);
    }

    void main() {
        vec3 color = rgbShift(uTexture, vUv, uOffset * 10.);
        gl_FragColor = vec4(color, uAlpha);

    }
`;

//vertex shader for post processing
const postVertexShader = `
    varying vec2 vUv;
    void main() {
        vUv = uv;
        gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
`;

//fragment shader for post processing
const postFragmentShader = `

    precision highp float;

    uniform sampler2D tDiffuse;
    uniform vec2 uResolution;
    uniform vec2 uMouse;
    uniform float uVelo;
    uniform float uAmount;
    uniform float uScrollVelo;

    varying vec2 vUv;

    float circle(vec2 uv, vec2 disc_center, float disc_radius, float border_size) {
        uv -= disc_center;
        uv*= uResolution;
        float dist = sqrt(dot(uv, uv));
        return smoothstep(disc_radius+border_size, disc_radius-border_size, dist);
    }
    float random( vec2 p ){
        vec2 K1 = vec2(    23.14069263277926,    2.665144142690225  );
        return fract( cos( dot(p,K1) ) * 12345.6789 );
    }
    void main() {
        vec2 newUV = vUv;

        vec2 scrollCPositionLeft = vec2(0, 0.5);
        vec2 scrollCPositionMiddle = vec2(0.5, 0.5);
        vec2 scrollCPositionRight = vec2(1, 0.5);

        float c = circle(newUV, uMouse, 0.0, 0.3);

        float scrollCLeft = circle(newUV, scrollCPositionLeft, 0.0, 0.6);
        float scrollCMiddle = circle(newUV, scrollCPositionMiddle, 0.0, 0.6);
        float scrollCRight = circle(newUV, scrollCPositionRight, 0.0, 0.6);

        float r = texture2D(tDiffuse, newUV.xy += c * (uVelo * .6) + scrollCLeft * (uScrollVelo / 50.) + scrollCMiddle * (uScrollVelo / 50.) + scrollCRight * (uScrollVelo / 50.)).x;
        float g = texture2D(tDiffuse, newUV.xy += c * (uVelo * .625) + scrollCLeft * (uScrollVelo / 50.) + scrollCMiddle * (uScrollVelo / 50.) + scrollCRight * (uScrollVelo / 50.)).y;
        float b = texture2D(tDiffuse, newUV.xy += c * (uVelo * .65) + scrollCLeft * (uScrollVelo / 50.) + scrollCMiddle * (uScrollVelo / 50.) + scrollCRight * (uScrollVelo / 50.)).z;
        float a = texture2D(tDiffuse, newUV.xy+c*(uVelo)).w;

        vec4 newColor = vec4(r, g, b, r+g+b+a);

        //controls grains
        newUV.y *= random(vec2(newUV.y, uAmount));
        newColor.rgb += random(newUV)* 0.10;
        
        gl_FragColor = newColor;
    }
`;

const clock = new THREE.Clock();

// clamp between min and max values
const clamp = function (number, min, max) {
    number = Number(number);
    return Math.min(max, Math.max(min, isNaN(number) ? min : number));
};

const lerp = function (p1, p2, t) {
    return p1 + (p2 - p1) * t
}

window.onload = (e) => {

    // NORMALIZE WHEEL

    slider = document.querySelector(".slider");
    sliderFirst = document.querySelector(".first-slider");
    sliderSecond = document.querySelector(".second-slider");

    scroll = {
        current: 0,
        target: 0,
        ease: 0.1
    };

    // scrollSpeed and targetScrollSpeed to pass as uniforms
    scrollSpeed = 0;
    targetScrollSpeed = 0;

    // used for uVelo calc inside getSpeed() for post fragment shader
    targetSpeed = 0;

    // touch event 
    isDown = false;

    addEvents();
    onResize();
    createRAF();
    
    // THREE

    container = document.querySelector('#app');
    images = [...document.querySelectorAll('.slider-image')];
    meshItems = []; // Used to store all meshes we will be creating.

    uViewportSizes = new THREE.Vector2();
    //mouseover distort
    mouse = new THREE.Vector2();
    prevMouse = new THREE.Vector2();
    followMouse = new THREE.Vector2();

    viewport();
    setupCamera();
    render();
    createGeometry();

    function addEvents() {
        window.addEventListener('mousewheel', onMouseWheel);
        window.addEventListener('wheel', onMouseWheel);

        window.addEventListener('mousemove', onMouseMove);
        
        window.addEventListener('mousedown', onTouchDown);
        window.addEventListener('mousemove', onTouchMove);
        window.addEventListener('mouseup', onTouchUp);

        window.addEventListener('touchstart', onTouchDown);
        window.addEventListener('touchmove', onTouchMove);
        window.addEventListener('touchend', onTouchUp);

        window.addEventListener('resize', onResize);
    }

    function onMouseWheel(e) {
        const normalized = normalizeWheel(e);
        const { pixelY } = normalized;

        scroll.target += pixelY;
    }

    function onTouchDown (e) {
        isDown = true
        scroll.position = scroll.current
        start = e.touches ? event.touches[0].clientX : e.clientX
    }
    
    function onTouchMove (e) {
        if (!isDown) return
        const x = e.touches ? event.touches[0].clientX : e.clientX
        const distance = (this.start - x) * 2
        this.scroll.target = this.scroll.position + distance
    }
    
    function onTouchUp (e) {
        isDown = false
    }

    function onResize() {
        sliderBounds = slider.getBoundingClientRect();
        viewport();
    }

    function onMouseMove(event) {
        mouse.x = event.clientX / window.innerWidth;
        mouse.y = 1 - event.clientY / window.innerHeight;
        getSpeed();
    }

    function createRAF() {
        requestAnimationFrame(update);
    }

    // Calcualting scroll speed
    function calculateScrollSpeed() {
        scrollSpeed = (scroll.target - scroll.current) / 500; 
        targetScrollSpeed -= .4 * (targetScrollSpeed - scrollSpeed); 
        targetScrollSpeed = clamp(targetScrollSpeed, -1.5, 1.5);
    }

    //get mouse event scrolls
    function getSpeed() {
        mouseSpeed = Math.sqrt((prevMouse.x - mouse.x) ** 2 + (prevMouse.y - mouse.y) ** 2);
        targetSpeed -= 0.1 * (targetSpeed - mouseSpeed);
        followMouse.x -= 0.1 * (followMouse.x - mouse.x);
        followMouse.y -= 0.1 * (followMouse.y - mouse.y);
        targetSpeed = clamp(targetSpeed, 0, 0.05);
    
        prevMouse.x = mouse.x;
        prevMouse.y = mouse.y;
    }

    function update() {
        scroll.current += (scroll.target - scroll.current) * scroll.ease;

        // arbitrary value to allow more scroll
        const multipliedScroll = scroll.current * 1.5;
        const multipliedIndex = (multipliedScroll / sliderBounds.width) * 2;
        multiplier = Math.floor(multipliedIndex);

        sliderFirst.style.transform = `translateX(${-multipliedScroll + (multiplier * sliderBounds.width) / 2}px)`;

        sliderSecond.style.transform = `translateX(${-multipliedScroll + (multiplier * sliderBounds.width) / 2}px)`;

        requestAnimationFrame(update);
    }

    // Getter function used to get screen dimensions used for the camera and mesh materials
    function viewport(){
        let width = window.innerWidth;
        let height = window.innerHeight;
        let aspectRatio = width / height;
        return {
          width,
          height,
          aspectRatio
        };
    }

    // reusable geometry instance (saves memory) 
    function createGeometry(){
        geometry = new THREE.PlaneBufferGeometry(1,1,30,30);
    }

    //setup camera and shaders
    function setupCamera(){
        // window.addEventListener('resize', this.onWindowResize.bind(this), false);
    
        // Create new scene
        scene = new THREE.Scene();
    
        // Initialize perspective camera
        let perspective = 1000;
        const fov = (180 * (2 * Math.atan(window.innerHeight / 2 / perspective))) / Math.PI; // see fov image for a picture breakdown of fov setting.
        camera = new THREE.PerspectiveCamera(fov, viewport().aspectRatio, 0.1, 2000);
        camera.position.set(0, 0, perspective); // set the camera position on the z axis.
        
        // renderer
        renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
        renderer.setSize(viewport().width, viewport().height); // uses the getter viewport function above to set size of canvas / renderer
        renderer.setPixelRatio(window.devicePixelRatio); // Import to ensure image textures do not appear blurred.
        container.appendChild(renderer.domElement); // append the canvas to the main element

        // Effect composer used to use custom shaders
        composer = new THREE.EffectComposer( renderer );
        composer.addPass(new THREE.RenderPass(scene, camera));
        
        // grain shader and color distort
        postUniforms = {
            "uResolution": {
                value: new THREE.Vector2(1, window.innerHeight / window.innerWidth)
            },
            "tDiffuse": {
                // value: null
            },
            "uMouse": {
                value: new THREE.Vector2()
            },
            "uVelo": {
                value: 0
            },
            "uAmount": {
                value: 0
            },
            "uScrollVelo": {
                value: 0
            },
            "uViewportSizes": {
                value: [0, 0]
            },
        };
        grainShader = new THREE.ShaderPass({
            uniforms: postUniforms,
            vertexShader: postVertexShader,
            fragmentShader: postFragmentShader,
            
        });
        composer.addPass(grainShader);
        grainShader.renderToScreen = true;
    }

    function render(){
        let elapsedTime = clock.getElapsedTime();
        
        getSpeed();
        calculateScrollSpeed();
        // console.log(targetScrollSpeed);

        // update mesh item uniforms and call render()
        meshItems.forEach((t) => {
            t.material.uniforms["uViewportSizes"].x = viewport().width;
            t.material.uniforms["uViewportSizes"].y = viewport().height;

            t.material.uniforms.uOffset.value.set((scroll.target - scroll.current) * 0.0002,  -(scroll.target - scroll.current) * 0.0000 );
            // console.log(t.material.uniforms.uOffset.value);
            t.material.uniforms["uScrollVelo"].value = targetScrollSpeed;
            
            t.render();
        });

        // update post processing uniforms
        grainShader.uniforms["uVelo"].value = targetSpeed;
        grainShader.uniforms["uAmount"].value = elapsedTime;
        grainShader.uniforms["uScrollVelo"].value = targetScrollSpeed;
        grainShader.uniforms["uMouse"].value = followMouse;


        composer.render();
        requestAnimationFrame(render);
    } 


    // MESH ITEMS

    class MeshItem{
        // Pass in the scene as we will be adding meshes to this scene.
        constructor(element, scene, geometry){
            this.element = element;
            this.scene = scene;
            this.geometry = geometry;
            this.offset = new THREE.Vector2(0,0); // Positions of mesh on screen. Will be updated below.
            this.sizes = new THREE.Vector2(0,0); //Size of mesh on screen. Will be updated below.
            // this.createGeometry();
            this.createMesh();
        }
    
        getDimensions(){
            const {width, height, top, left} = this.element.getBoundingClientRect();
            this.sizes.set(width, height);
            this.offset.set(left - window.innerWidth / 2 + width / 2, -top + window.innerHeight / 2 - height / 2); 
        }
    
        createMesh(){
            this.imageTexture = new THREE.TextureLoader().load(this.element.src);
            this.uniforms = {
                uTexture: {
                    //texture data
                    value: this.imageTexture
                },
                uOffset: {
                    //distortion strength
                    value: new THREE.Vector2(0.0, 0.0)
                },
                uAlpha: {
                    //opacity
                    value: 1.0
                },
                uSpeed: { 
                    value: 0 
                },
                uTime: { 
                    value: 0 
                },
                uScrollVelo: {
                    value: 0
                },
                uViewportSizes: {
                    value: [0, 0]
                },
            };
            this.material = new THREE.ShaderMaterial({
                uniforms: this.uniforms,
                vertexShader: imageVertexShader,
                fragmentShader: imageFragmentShader,
                transparent: true,
                // needsUpdate: true
                // side: THREE.DoubleSide
            })
            this.mesh = new THREE.Mesh( this.geometry, this.material );
            this.getDimensions(); // set offset and sizes for placement on the scene
            this.mesh.position.set(this.offset.x, this.offset.y, 0);
            this.mesh.scale.set(this.sizes.x, this.sizes.y, 1);
    
            this.scene.add( this.mesh );
        }
    
        render(){
            // this function is repeatidly called for each instance in the above 
            this.getDimensions();
            this.mesh.position.set(this.offset.x, this.offset.y, 0);
        }
    }

    // call MeshItem class after init
    createMeshItems();

    function createMeshItems(){
        // Loop thorugh all images and create new MeshItem instances. Push these instances to the meshItems array.
        images.forEach(image => {
            let meshItem = new MeshItem(image, scene, geometry);
            meshItems.push(meshItem);
        })
    }

};

  